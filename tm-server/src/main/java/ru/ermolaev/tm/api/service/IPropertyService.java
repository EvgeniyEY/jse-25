package ru.ermolaev.tm.api.service;

public interface IPropertyService {

    String getServerHost();

    Integer getServerPort();

    String getSessionSalt();

    Integer getSessionCycle();

    String getJdbcDriver();

    String getJdbcUrl();

    String getJdbcUsername();

    String getJdbcPassword();

}
