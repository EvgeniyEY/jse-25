package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    Long count();

    User findOneById(@NotNull String id);

    User findOneByLogin(@NotNull String login);

    List<User> findAll();

    void removeOneById(@NotNull String id);

    void removeOneByLogin(@NotNull String login);

    void removeAll();

}
