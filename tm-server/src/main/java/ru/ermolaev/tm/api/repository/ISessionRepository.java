package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    Long count();

    Long countByUserId(@NotNull String userId);

    Session findOneById(@NotNull String id);

    Session findOneByUserId(@NotNull String userId);

    List<Session> findAll();

    List<Session> findAllByUserId(@NotNull String userId);

    void removeOneById(@NotNull String id);

    void removeOneByUserId(@NotNull String userId);

    void removeAll();

    void removeAllByUserId(@NotNull String userId);

    boolean contains(@NotNull String id);

}
