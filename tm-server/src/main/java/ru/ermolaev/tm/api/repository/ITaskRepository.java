package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    Long count();

    @NotNull
    Long countByUserId(@NotNull String userId);

    @NotNull
    Long countByProjectId(@NotNull String projectId);

    @NotNull
    Long countByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Task findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String projectId);

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    void removeOneById(@NotNull String userId, @NotNull String id);

    void removeOneByName(@NotNull String userId, @NotNull String name);

    void removeAll();

    void removeAllByUserId(@NotNull String userId);

    void removeAllByProjectId(@NotNull String projectId);

    void removeAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
