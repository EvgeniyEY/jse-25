package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Project e", Long.class).getSingleResult();
    }

    @NotNull
    @Override
    public Long countByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM Project e WHERE e.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.id = :id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) {
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.name = :name", Project.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult();
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return entityManager.createQuery("SELECT e FROM Project e", Project.class).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId (@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        entityManager.createQuery("DELETE FROM Project e WHERE e.user.id = :userId AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) {
        entityManager.createQuery("DELETE FROM Project e WHERE e.user.id = :userId AND e.name = :name")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Project").executeUpdate();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Project e WHERE e.user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
