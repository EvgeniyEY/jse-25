package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Task e", Long.class).getSingleResult();
    }

    @NotNull
    @Override
    public Long countByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM Task e WHERE e.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Long countByProjectId(@NotNull final String projectId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM Task e WHERE e.project.id = :projectId", Long.class)
                .setParameter("projectId", projectId)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Long countByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM Task e WHERE e.user.id = :userId AND e.project.id = :projectId", Long.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Task findOneByName(@NotNull final String userId, @NotNull final String name) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.name = :name", Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult();
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("SELECT e FROM Task e", Task.class).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId (@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String projectId) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.project.id = :projectId", Task.class)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.project.id = :projectId", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        entityManager.createQuery("DELETE FROM Task e WHERE e.user.id = :userId AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) {
        entityManager.createQuery("DELETE FROM Task e WHERE e.user.id = :userId AND e.name = :name")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Task").executeUpdate();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Task e WHERE e.user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeAllByProjectId(@NotNull final String projectId) {
        entityManager.createQuery("DELETE FROM Task e WHERE e.project.id = :projectId")
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void removeAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId) {
        entityManager.createQuery("DELETE FROM Task e WHERE e.user.id = :userId AND e.project.id = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
