package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.api.repository.ISessionRepository;
import ru.ermolaev.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Session e", Long.class).getSingleResult();
    }

    @Override
    public Long countByUserId(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM Session e WHERE e.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public Session findOneById(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public Session findOneByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public List<Session> findAll() {
        return entityManager.createQuery("SELECT e FROM Session e", Session.class).getResultList();
    }

    @Override
    public List<Session> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.createQuery("DELETE FROM Session e WHERE e.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByUserId(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Session e WHERE e.user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Session").executeUpdate();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Session e WHERE e.user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean contains(@NotNull final String id) {
        @NotNull final Session session = findOneById(id);
        return findAll().contains(session);
    }

}
