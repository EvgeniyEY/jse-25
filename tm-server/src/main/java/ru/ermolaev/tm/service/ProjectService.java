package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.exception.incorrect.IncorrectCompleteDateException;
import ru.ermolaev.tm.exception.incorrect.IncorrectStartDateException;
import ru.ermolaev.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void persist(@Nullable Project project) {
        if (project == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            repository.persist(project);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@Nullable Project project) {
        if (project == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            repository.merge(project);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(@Nullable Project project) {
        if (project == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            repository.remove(project);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void createProject(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(serviceLocator.getUserService().findOneById(userId));
        persist(project);
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyDescriptionException();
        @NotNull final Project project = findOneById(userId, id);
        if (!name.isEmpty()) project.setName(name);
        if (!description.isEmpty()) project.setDescription(description);
        if (name.isEmpty() && description.isEmpty()) return;
        merge(project);
    }

    @Override
    public void updateStartDate(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Date date
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @NotNull final Project project = findOneById(userId, id);
        if (date.before(new Date(System.currentTimeMillis()))) throw new IncorrectStartDateException(date);
        project.setStartDate(date);
        merge(project);
    }

    @Override
    public void updateCompleteDate(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Date date
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @NotNull final Project project = findOneById(userId, id);
        if (project.getStartDate() == null) throw new IncorrectCompleteDateException(date);
        if (project.getStartDate().after(date)) throw new IncorrectCompleteDateException(date);
        project.setCompleteDate(date);
        merge(project);
    }

    @NotNull
    @Override
    public Long countAllProjects() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Long countOfProjects = repository.count();
        entityManager.close();
        return countOfProjects;
    }

    @NotNull
    @Override
    public Long countUserProjects(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Long countOfProjects = repository.countByUserId(userId);
        entityManager.close();
        return countOfProjects;
    }

    @NotNull
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Project project = repository.findOneById(userId, id);
        entityManager.close();
        return project;
    }

    @NotNull
    @Override
    public Project findOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Project project = repository.findOneByName(userId, name);
        entityManager.close();
        return project;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectDTO> projectsDTO = ProjectDTO.toDTO(repository.findAll());
        entityManager.close();
        return projectsDTO;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectDTO> projectsDTO = ProjectDTO.toDTO(repository.findAllByUserId(userId));
        entityManager.close();
        return projectsDTO;
    }

    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        repository.removeOneById(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        repository.removeOneByName(userId, name);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        repository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        repository.removeAllByUserId(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
