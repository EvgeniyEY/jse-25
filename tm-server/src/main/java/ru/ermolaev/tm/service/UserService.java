package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.repository.IUserRepository;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.dto.UserDTO;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.repository.UserRepository;
import ru.ermolaev.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void persist(@Nullable final User user) {
        if (user == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            repository.persist(user);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            repository.merge(user);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(@Nullable User user) {
        if (user == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            repository.remove(user);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        final User user = repository.findOneById(id);
        entityManager.close();
        return user;
    }

    @Nullable
    @Override
    public User findOneByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        final User user = repository.findOneByLogin(login);
        entityManager.close();
        return user;
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        final List<User> users = repository.findAll();
        entityManager.close();
        return UserDTO.toDTO(users);
    }

    @Override
    public void removeOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        repository.removeOneById(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeOneByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        repository.removeOneByLogin(login);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        repository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @Nullable final String saltPass = HashUtil.hidePassword(password);
        if (saltPass == null) return;
        user.setPasswordHash(saltPass);
        persist(user);
    }

    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        @Nullable final String saltPass = HashUtil.hidePassword(password);
        if (saltPass == null) return;
        user.setPasswordHash(saltPass);
        persist(user);
    }

    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @Nullable final String saltPass = HashUtil.hidePassword(password);
        if (saltPass == null) return;
        user.setPasswordHash(saltPass);
        user.setRole(role);
        persist(user);
    }

    @Nullable
    @Override
    public Long count() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        final Long count = repository.count();
        entityManager.close();
        return count;
    }

    @Override
    public void updatePassword(
            @Nullable final String userId,
            @Nullable final String newPassword
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPasswordException();
        @Nullable final String saltPass = HashUtil.hidePassword(newPassword);
        if (saltPass == null) return;
        final User user = findOneById(userId);
        user.setPasswordHash(saltPass);
        merge(user);
    }

    @Override
    public void updateUserFirstName(
            @Nullable final String userId,
            @Nullable final String newFirstName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyFirstNameException();
        final User user = findOneById(userId);
        user.setFirstName(newFirstName);
        merge(user);
    }

    @Override
    public void updateUserMiddleName(
            @Nullable final String userId,
            @Nullable final String newMiddleName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new EmptyMiddleNameException();
        final User user = findOneById(userId);
        user.setMiddleName(newMiddleName);
        merge(user);
    }

    @Override
    public void updateUserLastName(
            @Nullable final String userId,
            @Nullable final String newLastName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newLastName == null || newLastName.isEmpty()) throw new EmptyLastNameException();
        final User user = findOneById(userId);
        user.setLastName(newLastName);
        merge(user);
    }

    @Override
    public void updateUserEmail(
            @Nullable final String userId,
            @Nullable final String newEmail
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newEmail == null || newEmail.isEmpty()) throw new EmptyEmailException();
        final User user = findOneById(userId);
        user.setEmail(newEmail);
        merge(user);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        final User user = findOneByLogin(login);
        user.setLocked(true);
        merge(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        final User user = findOneByLogin(login);
        user.setLocked(false);
        merge(user);
    }

}
