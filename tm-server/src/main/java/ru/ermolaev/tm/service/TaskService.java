package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.dto.TaskDTO;
import ru.ermolaev.tm.entity.Task;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.exception.incorrect.IncorrectCompleteDateException;
import ru.ermolaev.tm.exception.incorrect.IncorrectStartDateException;
import ru.ermolaev.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void persist(@Nullable Task task) {
        if (task == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            repository.persist(task);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@Nullable Task task) {
        if (task == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            repository.merge(task);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(@Nullable Task task) {
        if (task == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            repository.remove(task);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void createTask(
            @Nullable final String userId,
            @Nullable final String taskName,
            @Nullable final String projectName,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskName == null || taskName.isEmpty()) throw new EmptyNameException();
        if (projectName == null || projectName.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setDescription(description);
        task.setUser(serviceLocator.getUserService().findOneById(userId));
        task.setProject(serviceLocator.getProjectService().findOneByName(userId, projectName));
        persist(task);
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyDescriptionException();
        @NotNull final Task task = findOneById(userId, id);
        if (!name.isEmpty()) task.setName(name);
        if (!description.isEmpty()) task.setDescription(description);
        if (name.isEmpty() && description.isEmpty()) return;
        merge(task);
    }

    @Override
    public void updateStartDate(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Date date
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @NotNull final Task task = findOneById(userId, id);
        if (date.before(new Date(System.currentTimeMillis()))) throw new IncorrectStartDateException(date);
        task.setStartDate(date);
        merge(task);
    }

    @Override
    public void updateCompleteDate(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Date date
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @NotNull final Task task = findOneById(userId, id);
        if (task.getStartDate() == null) throw new IncorrectCompleteDateException(date);
        if (task.getStartDate().after(date)) throw new IncorrectCompleteDateException(date);
        task.setCompleteDate(date);
        merge(task);
    }

    @NotNull
    @Override
    public Long countAllTasks() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Long countOfTasks = repository.count();
        entityManager.close();
        return countOfTasks;
    }

    @NotNull
    @Override
    public Long countByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Long countOfTasks = repository.countByUserId(userId);
        entityManager.close();
        return countOfTasks;
    }

    @NotNull
    @Override
    public Long countByProjectId(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Long countOfTasks = repository.countByProjectId(projectId);
        entityManager.close();
        return countOfTasks;
    }

    @NotNull
    @Override
    public Long countByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Long countOfTasks = repository.countByUserIdAndProjectId(userId, projectId);
        entityManager.close();
        return countOfTasks;
    }

    @NotNull
    @Override
    public Task findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Task task = repository.findOneById(userId, id);
        entityManager.close();
        return task;
    }

    @NotNull
    @Override
    public Task findOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Task task = repository.findOneByName(userId, name);
        entityManager.close();
        return task;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<TaskDTO> tasksDTO = TaskDTO.toDTO(repository.findAll());
        entityManager.close();
        return tasksDTO;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<TaskDTO> tasksDTO = TaskDTO.toDTO(repository.findAllByUserId(userId));
        entityManager.close();
        return tasksDTO;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<TaskDTO> tasksDTO = TaskDTO.toDTO(repository.findAllByProjectId(projectId));
        entityManager.close();
        return tasksDTO;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<TaskDTO> tasksDTO = TaskDTO.toDTO(repository.findAllByUserIdAndProjectId(userId, projectId));
        entityManager.close();
        return tasksDTO;
    }

    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeOneById(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeOneByName(userId, name);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeAllByUserId(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllByProjectId(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeAllByProjectId(projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeAllByUserIdAndProjectId(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
