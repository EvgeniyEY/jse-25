package ru.ermolaev.tm.endpoint;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.junit.Assert;
import ru.ermolaev.tm.bootstrap.Bootstrap;

public class ProjectDTOEndpointTest {

    private final Bootstrap bootstrap = new Bootstrap();

    private final String projectName = "projectForTest";

    private final String projectDescription = "projectForTest";

    private ProjectDTO testProject;

    @Before
    public void prepareToTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        bootstrap.getSessionService().setCurrentSession(session);
        bootstrap.getProjectEndpoint().createProject(session, projectName, projectDescription);
        testProject = bootstrap.getProjectEndpoint().showProjectByName(session, projectName);
    }

    @After
    public void clearAfterTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        bootstrap.getProjectEndpoint().clearProjects(session);
        bootstrap.getSessionEndpoint().closeSession(session);
        bootstrap.getSessionService().clearCurrentSession();
        testProject = null;
    }

    @Test
    public void createProjectTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final ProjectDTO project = bootstrap.getProjectEndpoint().showProjectByName(session, projectName);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectName, project.getName());
        Assert.assertEquals(projectDescription, project.getDescription());
    }

    @Test
    public void clearAndShowAllProjectsTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        for (int i = 0; i < 4; i++) {
            bootstrap.getProjectEndpoint().createProject(session, "testProject" + i, "description" + i);
        }
        Assert.assertEquals(5, bootstrap.getProjectEndpoint().showAllProjects(session).size());
        bootstrap.getProjectEndpoint().clearProjects(session);
        Assert.assertEquals(0, bootstrap.getProjectEndpoint().showAllProjects(session).size());
    }

    @Test
    public void showProjectByNameTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final ProjectDTO project = bootstrap.getProjectEndpoint().showProjectByName(session, projectName);
        Assert.assertNotNull(project);
        Assert.assertNotNull(testProject);
        Assert.assertEquals(testProject.getId(), project.getId());
    }

    @Test
    public void showProjectByIdTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final ProjectDTO project = bootstrap.getProjectEndpoint().showProjectById(session, testProject.getId());
        Assert.assertNotNull(project);
        Assert.assertNotNull(testProject);
        Assert.assertEquals(testProject.getId(), project.getId());

    }

    @Test
    public void updateProjectByIdTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final ProjectDTO project = bootstrap.getProjectEndpoint().updateProjectById(session, testProject.getId(), "updatedTestProject", "TestDesc");
        Assert.assertNotNull(project);
        Assert.assertEquals("updatedTestProject", project.getName());
        Assert.assertEquals("TestDesc", project.getDescription());
    }

    @Test
    public void removeProjectByIdTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        bootstrap.getProjectEndpoint().createProject(session, "testProjectForRemove", projectDescription);
        final ProjectDTO createdProject = bootstrap.getProjectEndpoint().showProjectByName(session, "testProjectForRemove");
        Assert.assertNotNull(createdProject);
        final ProjectDTO removedProject = bootstrap.getProjectEndpoint().removeProjectById(session, createdProject.getId());
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(createdProject.getId(), removedProject.getId());
    }

    @Test
    public void removeProjectByNameTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        bootstrap.getProjectEndpoint().createProject(session, "testProjectForRemove", projectDescription);
        final ProjectDTO createdProject = bootstrap.getProjectEndpoint().showProjectByName(session, "testProjectForRemove");
        Assert.assertNotNull(createdProject);
        final ProjectDTO removedProject = bootstrap.getProjectEndpoint().removeProjectByName(session, "testProjectForRemove");
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(createdProject.getId(), removedProject.getId());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void createProjectWithNullSessionTest() throws Exception_Exception {
        bootstrap.getProjectEndpoint().createProject(null, projectName, projectDescription);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void createProjectWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getProjectEndpoint().createProject(fakeSession, projectName, projectDescription);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearProjectsWithNullSessionTest() throws Exception_Exception {
        bootstrap.getProjectEndpoint().clearProjects(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearProjectsWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getProjectEndpoint().clearProjects(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showAllProjectsWithNullSessionTest() throws Exception_Exception {
        bootstrap.getProjectEndpoint().showAllProjects(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showAllProjectsWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getProjectEndpoint().showAllProjects(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showProjectByIdWithNullSessionTest() throws Exception_Exception {
        bootstrap.getProjectEndpoint().showProjectById(null, testProject.getId());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showProjectByIdWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getProjectEndpoint().showProjectById(fakeSession, testProject.getId());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showProjectByNameWithNullSessionTest() throws Exception_Exception {
        bootstrap.getProjectEndpoint().showProjectByName(null, testProject.getName());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showProjectByNameWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getProjectEndpoint().showProjectByName(fakeSession, testProject.getName());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateProjectByIdWithNullSessionTest() throws Exception_Exception {
        bootstrap.getProjectEndpoint().updateProjectById(null, testProject.getId(), "up", "up");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateProjectByIdWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getProjectEndpoint().updateProjectById(fakeSession, testProject.getId(), "up", "up");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void removeProjectByIdWithNullSessionTest() throws Exception_Exception {
        bootstrap.getProjectEndpoint().removeProjectById(null, testProject.getId());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void removeProjectByIdWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getProjectEndpoint().removeProjectById(fakeSession, testProject.getId());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void removeProjectByNameWithNullSessionTest() throws Exception_Exception {
        bootstrap.getProjectEndpoint().removeProjectByName(null, testProject.getName());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void removeProjectByNameWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getProjectEndpoint().removeProjectByName(fakeSession, testProject.getName());
    }

}
