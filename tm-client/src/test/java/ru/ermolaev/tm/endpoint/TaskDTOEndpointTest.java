package ru.ermolaev.tm.endpoint;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.junit.Assert;
import ru.ermolaev.tm.bootstrap.Bootstrap;

public class TaskDTOEndpointTest {

    private final Bootstrap bootstrap = new Bootstrap();

    private final String taskName = "taskForTest";

    private final String taskDescription = "taskForTest";

    private TaskDTO testTask;

    @Before
    public void prepareToTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        bootstrap.getSessionService().setCurrentSession(session);
        bootstrap.getTaskEndpoint().createTask(session, taskName, taskDescription);
        testTask = bootstrap.getTaskEndpoint().showTaskByName(session, taskName);
    }

    @After
    public void clearAfterTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        bootstrap.getTaskEndpoint().clearTasks(session);
        bootstrap.getSessionEndpoint().closeSession(session);
        bootstrap.getSessionService().clearCurrentSession();
        testTask = null;
    }

    @Test
    public void createTaskTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final TaskDTO task = bootstrap.getTaskEndpoint().showTaskByName(session, taskName);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskName, task.getName());
        Assert.assertEquals(taskDescription, task.getDescription());
    }

    @Test
    public void clearAndShowAllTasksTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        for (int i = 0; i < 4; i++) {
            bootstrap.getTaskEndpoint().createTask(session, "testTask" + i, "description" + i);
        }
        Assert.assertEquals(5, bootstrap.getTaskEndpoint().showAllTasks(session).size());
        bootstrap.getTaskEndpoint().clearTasks(session);
        Assert.assertEquals(0, bootstrap.getTaskEndpoint().showAllTasks(session).size());
    }

    @Test
    public void showTaskByNameTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final TaskDTO task = bootstrap.getTaskEndpoint().showTaskByName(session, taskName);
        Assert.assertNotNull(task);
        Assert.assertNotNull(testTask);
        Assert.assertEquals(testTask.getId(), task.getId());
    }

    @Test
    public void showTaskByIdTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final TaskDTO task = bootstrap.getTaskEndpoint().showTaskById(session, testTask.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(testTask);
        Assert.assertEquals(testTask.getId(), task.getId());

    }

    @Test
    public void updateTaskByIdTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final TaskDTO task = bootstrap.getTaskEndpoint().updateTaskById(session, testTask.getId(), "updatedTestTask", "TestDesc");
        Assert.assertNotNull(task);
        Assert.assertEquals("updatedTestTask", task.getName());
        Assert.assertEquals("TestDesc", task.getDescription());
    }

    @Test
    public void removeTaskByIdTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        bootstrap.getTaskEndpoint().createTask(session, "testTaskForRemove", taskDescription);
        final TaskDTO createdTask = bootstrap.getTaskEndpoint().showTaskByName(session, "testTaskForRemove");
        Assert.assertNotNull(createdTask);
        final TaskDTO removedTask = bootstrap.getTaskEndpoint().removeTaskById(session, createdTask.getId());
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(createdTask.getId(), removedTask.getId());
    }

    @Test
    public void removeTaskByNameTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        bootstrap.getTaskEndpoint().createTask(session, "testTaskForRemove", taskDescription);
        final TaskDTO createdTask = bootstrap.getTaskEndpoint().showTaskByName(session, "testTaskForRemove");
        Assert.assertNotNull(createdTask);
        final TaskDTO removedTask = bootstrap.getTaskEndpoint().removeTaskByName(session, "testTaskForRemove");
        Assert.assertNotNull(removedTask);
        Assert.assertEquals(createdTask.getId(), removedTask.getId());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void createTaskWithNullSessionTest() throws Exception_Exception {
        bootstrap.getTaskEndpoint().createTask(null, taskName, taskDescription);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void createTaskWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getTaskEndpoint().createTask(fakeSession, taskName, taskDescription);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearTasksWithNullSessionTest() throws Exception_Exception {
        bootstrap.getTaskEndpoint().clearTasks(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearTasksWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getTaskEndpoint().clearTasks(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showAllTasksWithNullSessionTest() throws Exception_Exception {
        bootstrap.getTaskEndpoint().showAllTasks(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showAllTasksWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getTaskEndpoint().showAllTasks(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showTaskByIdWithNullSessionTest() throws Exception_Exception {
        bootstrap.getTaskEndpoint().showTaskById(null, testTask.getId());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showTaskByIdWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getTaskEndpoint().showTaskById(fakeSession, testTask.getId());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showTaskByNameWithNullSessionTest() throws Exception_Exception {
        bootstrap.getTaskEndpoint().showTaskByName(null, testTask.getName());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showTaskByNameWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getTaskEndpoint().showTaskByName(fakeSession, testTask.getName());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateTaskByIdWithNullSessionTest() throws Exception_Exception {
        bootstrap.getTaskEndpoint().updateTaskById(null, testTask.getId(), "up", "up");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateTaskByIdWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getTaskEndpoint().updateTaskById(fakeSession, testTask.getId(), "up", "up");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void removeTaskByIdWithNullSessionTest() throws Exception_Exception {
        bootstrap.getTaskEndpoint().removeTaskById(null, testTask.getId());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void removeTaskByIdWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getTaskEndpoint().removeTaskById(fakeSession, testTask.getId());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void removeTaskByNameWithNullSessionTest() throws Exception_Exception {
        bootstrap.getTaskEndpoint().removeTaskByName(null, testTask.getName());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void removeTaskByNameWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getTaskEndpoint().removeTaskByName(fakeSession, testTask.getName());
    }

}
