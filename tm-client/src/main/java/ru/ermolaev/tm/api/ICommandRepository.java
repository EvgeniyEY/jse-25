package ru.ermolaev.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    @NotNull
    List<AbstractCommand> getCommandList();

}
