package ru.ermolaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.SessionDTO;

public final class UserLogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout from your account.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getSessionEndpoint().closeSession(session);
        serviceLocator.getSessionService().clearCurrentSession();
        System.out.println("[OK]");
    }

}
