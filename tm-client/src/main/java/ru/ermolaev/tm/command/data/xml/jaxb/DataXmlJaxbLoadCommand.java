package ru.ermolaev.tm.command.data.xml.jaxb;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.enumeration.Role;

public final class DataXmlJaxbLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-xml-jb-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from XML (Jax-B) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML (JAX-B) LOAD]");
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getAdminDataEndpoint().loadXmlByJaxb(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
