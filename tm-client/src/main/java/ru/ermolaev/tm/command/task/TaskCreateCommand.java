package ru.ermolaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create a new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getTaskEndpoint().createTask(session, name, description);
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
